# golang-lib
[![DroneCI](https://ci.sum7.eu/api/badges/genofire/golang-lib/status.svg?branch=master)](https://ci.sum7.eu/genofire/golang-lib)
[![Travis](https://travis-ci.org/genofire/golang-lib.svg?branch=master)](https://travis-ci.org/genofire/golang-lib) [![CircleCI](https://circleci.com/gh/genofire/golang-lib/tree/master.svg?style=shield)](https://circleci.com/gh/genofire/golang-lib/tree/master)
[![Coverage](https://coveralls.io/repos/github/genofire/golang-lib/badge.svg?branch=master)](https://coveralls.io/github/genofire/golang-lib?branch=master)
[![codecov](https://codecov.io/gh/genofire/golang-lib/branch/master/graph/badge.svg)](https://codecov.io/gh/genofire/golang-lib)
[![Go Report Card](https://goreportcard.com/badge/dev.sum7.eu/genofire/golang-lib)](https://goreportcard.com/report/dev.sum7.eu/genofire/golang-lib)
[![GoDoc](https://godoc.org/dev.sum7.eu/genofire/golang-lib?status.svg)](https://godoc.org/dev.sum7.eu/genofire/golang-lib)

some packages collected for easy and often used functions
